<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Translation extends Model
{
    use HasFactory;
//    use SoftDeletes;

    protected $table = 'translations';

    protected $fillable = [
        'article_id',
        'name',
        'description',
        'meta_name',
        'meta_description',
        'delivery',
        'mounting',
        'returning',
        'lang_id',
        'model_name',
    ];
}
