<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $table = 'credits';

    protected $model = 'App\\Models\\Credit::class';

    protected $fillable = [
        'slug',
        'image'
    ];

    protected $with = ['lang', 'credits'];

    protected $appends = ['pluck_credits'];

    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id')->where('model_name', $this->model)->where('lang_id',app()->getLocale());
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id')->where('model_name', $this->model);
    }

    public function credits()
    {
        return $this->hasMany(CreditsAttributePivot::class, 'credit_id', 'id');
    }

    public function GetPluckCreditsAttribute()
    {
        return $this->credits()->select('interest_rate', 'term')->get();
    }
}
