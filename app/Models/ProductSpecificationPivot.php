<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSpecificationPivot extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['product_id', 'spec_id', 'attribute_id', 'specification_id'];

    protected $table = 'product_specification_pivot';

    protected $model = 'App\\Models\\ProductSpecificationPivot::class';

    protected $with = ['attribute'];

    public function attribute(){
        return $this->hasOne(Specification::class, 'id', 'specification_id');
    }
}
