<?php

namespace App\Models;

use Faker\Provider\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Blog extends Model
{
    protected $table = 'blogs';

    protected $model = 'App\\Models\\Blog::class';

    protected $fillable = [
        'slug',
        'status',
        'image'
        ];

    protected $with = ['lang'];

    protected $appends = ['status_name'];

    public function GetStatusNameAttribute(){
        return $this->status == 1 ? 'Active' : 'Inactive';
    }

    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id')->where('model_name', $this->model)->where('lang_id',app()->getLocale());
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id')->where('model_name', $this->model);
    }
}
