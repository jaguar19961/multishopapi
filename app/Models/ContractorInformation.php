<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContractorInformation extends Model
{
    use HasFactory;

    protected $table = 'contractor_informations';
}
