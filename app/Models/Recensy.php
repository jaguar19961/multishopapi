<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recensy extends Model
{
    protected $table = 'recensies';

    protected $fillable = [
        'image',
        'name',
        'description',
        'pozitions',
        'state'
    ];
}
