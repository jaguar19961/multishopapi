<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'orders';
    protected $model = 'App\\Models\\Order::class';
    protected $fillable = ['invoice_id', 'product_id', 'sku', 'qunatity', 'price', 'credit'];
    protected $with = ['productSpecification'];
    protected $casts = ['credit' => 'array'];

    public function productSpecification()
    {
        return $this->hasOne(ProductSpecification::class, 'id', 'product_id');
    }
}
