<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'invoices';
    protected $model = 'App\\Models\\Invoice::class';
    protected $fillable = ['address', 'delivery_home', 'delivery_method', 'email', 'first_name', 'last_name', 'locality', 'phone', 'take_office', 'terms', 'total', 'delivery_price'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'invoice_id', 'id');
    }

    public function statusName(){
        return $this->hasOne(Status::class, 'id', 'status');
    }

    public function local(){
        return $this->hasOne(Local::class, 'id', 'locality');
    }
}
