<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    use HasFactory;

    protected $table = 'about_us';

    protected $model = 'App\\Models\\AboutUs::class';

    protected $fillable = [
        'slug',
        'image_1',
        'image_2',
        'image_3',
        'image_4',
    ];

    protected $with = ['lang'];


    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id')->where('model_name', $this->model)->where('lang_id', app()->getLocale());
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id')->where('model_name', $this->model);
    }
}
