<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreditsAttributePivot extends Model
{
    use HasFactory;

    protected $table = 'credits_attribute_pivot';

    protected $model = 'App\\Models\\CreditsAttributePivot::class';

    protected $fillable = [
        'credit_id',
        'term',
        'interest_rate'
    ];

    public function creditFirm()
    {
        return $this->hasOne(Credit::class, 'id', 'credit_id');
    }


}
