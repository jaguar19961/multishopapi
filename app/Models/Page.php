<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $table = 'pages';

    protected $model = 'App\\Models\\Page::class';

    protected $fillable = [
        'slug',
        'status',
        'image'
    ];

    protected $with = ['lang'];

    protected $appends = ['status_name'];

    public function GetStatusNameAttribute(){
        return $this->status == 1 ? 'Active' : 'Inactive';
    }

    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id')->where('model_name', $this->model)->where('lang_id',app()->getLocale());
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id')->where('model_name', $this->model);
    }
}
