<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSpecification extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['locale_sku', 'sku', 'product_id', 'price', 'image', 'price_buing'];

    protected $table = 'product_specifications';

    protected $model = 'App\\Models\\ProductSpecification::class';

    protected $casts = ['sub_specs' => 'array'];

    protected $with = ['prodSpecs', 'colorSpec', 'galleries'];

    public function prodSpecs()
    {
        return $this->hasMany(ProductSpecificationPivot::class, 'spec_id', 'id');
    }

    public function colorSpec()
    {
        return $this->hasOne(ProductSpecificationPivot::class, 'spec_id', 'id')->where('attribute_id', 2);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function labels()
    {
        return $this->hasMany(ProductLabels::class, 'product_id', 'product_id');
    }

    public function galleries()
    {
        return $this->hasMany(ProductGallery::class, 'product_id', 'id');
    }
}
