<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    use HasFactory;

    protected $table = 'locals';

    protected $model = 'App\\Models\\Local::class';

    protected $fillable = [
        'id',
        'slug',
        'price',
    ];

    protected $with = ['lang'];

    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id')->where('model_name', $this->model)->where('lang_id',app()->getLocale());
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id')->where('model_name', $this->model);
    }
}
