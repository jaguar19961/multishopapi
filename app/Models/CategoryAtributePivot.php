<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryAtributePivot extends Model
{
    use HasFactory;

    protected $table = 'category_attribute_pivot';

    protected $model = 'App\\Models\\CategoryAtributePivot::class';

    protected $fillable = [
        'id',
        'category_id',
        'attribute_id',
    ];

    protected $with = ['atribute'];

    public function atribute()
    {
        return $this->hasOne(Specification::class, 'id', 'attribute_id');
    }
}
