<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['slug', 'name', 'category_id', 'price', 'offer_type_promotie', 'offer_type_populare', 'offer_type_new',
        'no_stock', 'furnizor_id'];

    protected $table = 'products';

    protected $model = 'App\\Models\\Product::class';

    protected $with = ['lang', 'productSpecifications', 'category', 'transMany', 'rates', 'labels'];

    protected $appends = ['calc_rates', 'pluck_label'];

    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id')->where('model_name', $this->model)->where('lang_id', app()->getLocale());
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id')->where('model_name', $this->model);
    }

    public function productSpecifications()
    {
        return $this->hasMany(ProductSpecification::class, 'product_id', 'id');
    }

    public function productSpecificationsAsc()
    {
        return $this->hasMany(ProductSpecification::class, 'product_id', 'id')->orderBy('price', 'asc');
    }

    public function productAttributes()
    {
        return $this->hasMany(ProductSpecificationPivot::class, 'product_id', 'id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function rates()
    {
        return $this->hasMany(Rate::class, 'product_id', 'id');
    }

    public function furnizor()
    {
        return $this->hasOne(Brand::class, 'id', 'furnizor_id');
    }

    public function labels()
    {
        return $this->hasMany(ProductLabels::class, 'product_id', 'id');
    }

    public function GetPluckLabelAttribute()
    {
        return $this->labels()->pluck('label_id')->toArray();
    }

    public function GetCalcRatesAttribute()
    {
        $rates = $this->rates();
        $count = $rates->count();
        if ($count > 0) {
            $sum = $rates->sum('rate');
            $total = $sum / $count;
            return $total;
        } else {
            return 0;
        }


    }
}
