<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Blog;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class AboutUsController extends Controller
{
    protected $model;
    protected $model_name;

    public function __construct(AboutUs $model,  Translation $translation)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->model_name = 'App\\Models\\AboutUs::class';
    }

    public function index()
    {
        $model = $this->model->orderBy('created_at', 'desc')->get();
        return view('auth.pages.about.index', compact('model'));
    }

    public function create(){
        return view('auth.pages.about.create');
    }

    public function show($id)
    {
        $model = $this->model->findOrFail($id);
        return view('auth.pages.about.show', compact('model'));
    }

    public function store(Request $request)
    {

        $input = $request->all();
        $model = $this->model::create($input);
        foreach ($request->name as $key => $lang) {
            $arr = array(
                'article_id' =>  $model->id,
                'name' =>  $lang ? $lang : null,
                'description' =>  $request->description[$key],
                'meta_name' =>  $request->meta_name[$key],
                'meta_description' =>  $request->meta_description[$key],
                'delivery' =>  $request->delivery[$key],
                'lang_id' => $key,
                'model_name' => $this->model_name,
            );
            $this->translation->create($arr);
        }

        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro'] . '-' . $model->id);
        if ($request->hasFile('image')) {
            $dir = '/img/blog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image_1 = $dir . $fileName;
        }
        if ($request->hasFile('image1')) {
            $dir = '/img/blog/';
            $extension = strtolower($request->file('image1')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image1')->move(public_path($dir), $fileName);
            $new->image_2 = $dir . $fileName;
        }
        if ($request->hasFile('image2')) {
            $dir = '/img/blog/';
            $extension = strtolower($request->file('image2')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image2')->move(public_path($dir), $fileName);
            $new->image_3 = $dir . $fileName;
        }
        if ($request->hasFile('image3')) {
            $dir = '/img/blog/';
            $extension = strtolower($request->file('image3')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image3')->move(public_path($dir), $fileName);
            $new->image_4 = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully Created!');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $model = $this->model::findOrFail($request->id);
        foreach ($request->name as $key => $lang) {
            $productNameTranslation = Translation::where('model_name', $this->model_name)->where('article_id', $request->id)->where('lang_id', $key)->first();
            $productNameTranslation->article_id = $request->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->description = $request['description'][$key];
            $productNameTranslation->meta_name = $request['meta_name'][$key];
            $productNameTranslation->meta_description = $request['meta_description'][$key];
            $productNameTranslation->delivery = $request['delivery'][$key];
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->model_name = $this->model_name;
            $productNameTranslation->save();
        }
        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro'] . '-' . $model->id);
        if ($request->hasFile('image')) {
            $dir = '/img/blog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image_1 = $dir . $fileName;
        }
        if ($request->hasFile('image1')) {
            $dir = '/img/blog/';
            $extension = strtolower($request->file('image1')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image1')->move(public_path($dir), $fileName);
            $new->image_2 = $dir . $fileName;
        }
        if ($request->hasFile('image2')) {
            $dir = '/img/blog/';
            $extension = strtolower($request->file('image2')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image2')->move(public_path($dir), $fileName);
            $new->image_3 = $dir . $fileName;
        }
        if ($request->hasFile('image3')) {
            $dir = '/img/blog/';
            $extension = strtolower($request->file('image3')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image3')->move(public_path($dir), $fileName);
            $new->image_4 = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        $model->delete();

        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }
}
