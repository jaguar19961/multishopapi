<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Specification;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;

class SpecificationController extends Controller
{
    protected $model;
    protected $model_name;
    protected $translation;

    public function __construct(Specification $model, Translation $translation)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->model_name = 'App\\Models\\Specification::class';
    }

    public function index($id)
    {
        $model = $this->model->orderBy('created_at', 'desc')->where('parent_id', $id)->get();
        $parent_name = $this->model->findOrFail($id);
        return view('auth.pages.specification.index', compact('model', 'parent_name'));
    }

    public function create()
    {
        $model = $this->model::get();
        $parent = $this->model::where('parent_id', 0)->get();
        return view('auth.pages.specification.create', compact('model', 'parent'));
    }

    public function show($id)
    {

        $model = $this->model->findOrFail($id);
        $parent = $this->model::where('parent_id', 0)->get();
        return view('auth.pages.specification.show', compact('model', 'parent'));
    }

    public function store(Request $request)
    {
        $exist = $this->model->where('slug', 'LIKE', $request->name['ro'])->where('parent_id', $request->parent_id)->first();
        if (!$exist) {
            $model = new $this->model();
            $model->save();
            foreach ($request->name as $key => $lang) {
                $arr = array(
                    'article_id' => $model->id,
                    'name' => $lang ? $lang : null,
                    'lang_id' => $key,
                    'model_name' => $this->model_name,
                );
                $this->translation->create($arr);
            }
            //----------
            $new = $this->model->findOrFail($model->id);
            $new->slug = Str::slug($request->name['ro']);
            $new->parent_id = $request->parent_id != null ? $request->parent_id : 0;
            $new->hex = $request->hex;
            if ($request->hasFile('image')) {
                $dir = '/img/category/';
                $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
                $fileName = Str::random() . '.' . $extension; // rename image
                $request->file('image')->move(public_path($dir), $fileName);
                $new->image = $dir . $fileName;
            }
            $new->save();
            Session::flash('flash_message', 'Successfully Created!');

        }else{
            Session::flash('flash_message', 'This specification Exists!');
        }
        return redirect()->back();

    }

    public function update(Request $request)
    {
        $model = $this->model::findOrFail($request->id);
        foreach ($request->name as $key => $lang) {
            $productNameTranslation = Translation::where('model_name', $this->model_name)->where('article_id', $request->id)->where('lang_id', $key)->first();
            $productNameTranslation->article_id = $request->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->model_name = $this->model_name;
            $productNameTranslation->save();
        }

        //----------
        //----------
        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro']);
        $new->parent_id = $request->parent_id != null ? $request->parent_id : 0;
        $new->hex = $request->hex;
        if ($request->hasFile('image')) {
            $dir = '/img/category/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        if ($model->productFilter->count() > 0){
            Session::flash('flash_message', 'This specification have '. $model->productFilter->count() .' products, deletion not posible!');
            return redirect()->back();
        }else{
            $relation = $model->transMany;
            foreach ($relation as $key => $trans){
                $trans->delete();
            }
            $model->delete();

            Session::flash('flash_message', 'Successfully deleted!');
            return redirect()->back();
        }

    }
}
