<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Script;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('auth.pages.dashboard');
    }

}
