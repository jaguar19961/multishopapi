<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Labels;
use App\Models\Product;
use App\Models\ProductGallery;
use App\Models\ProductOfferType;
use App\Models\ProductSpecification;
use App\Models\ProductSpecificationPivot;
use App\Models\Specification;
use App\Models\Translation;
use App\Models\ProductLabels;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\Console\Input\Input;

class ProductsController extends Controller
{
    protected $model;
    protected $model_name;
    protected $translation;
    protected $brand;

    public function __construct(Product $model, Translation $translation, Brand $brand)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->brand = $brand;
        $this->model_name = 'App\\Models\\Product::class';
    }

    public function index()
    {
        $model = $this->model->orderBy('created_at', 'desc');
        $model = $model->paginate(20);
        return view('auth.pages.products.index', compact('model'));
    }

    public function search(Request $request)
    {
        $input = $request->search;
//        dd($input);
        $model = $this->model->orderBy('created_at', 'desc');
        if ($input != null) {
            $model = $model->whereHas('transMany', function ($q) use ($input) {
                $q->where('name', 'LIKE', "%{$input}%");
            })->orWhereHas('productSpecifications', function ($qq) use ($input) {
                $qq->where('sku', $input);
            })->orWhereHas('productSpecifications', function ($qq) use ($input) {
                $qq->where('locale_sku', $input);
            })->orWhereHas('furnizor', function ($qqq) use ($input) {
                $qqq->where('name', $input);
            });
        }
        $model = $model->paginate(50);
        return view('auth.pages.products.index', compact('model'));
    }

    public function create()
    {
        $model = $this->model::get();
        $categories = Category::where('parent_id', 0)->get();
        $specifications = Specification::where('parent_id', 0)->with('parent')->get();
        $offer_types = ProductOfferType::get();
        $furnizor = $this->brand->get();
        $labels = Labels::get();
        return view('auth.pages.products.create', compact('model', 'categories', 'specifications', 'offer_types', 'furnizor', 'labels'));
    }

    public function show($id)
    {
        $model = $this->model->findOrFail($id);
        $categories = Category::where('parent_id', 0)->get();
        $specifications = Specification::where('parent_id', 0)->with('parent')->get();
        $offer_types = ProductOfferType::get();
        $parent_id = Category::findOrFail($model->category_id)->parent_id;
        $parent_category = Category::with('childs')->findOrFail($parent_id);
        $furnizor = $this->brand->get();
        $labels = Labels::get();
        return view('auth.pages.products.show', compact('parent_category', 'model', 'labels', 'categories', 'specifications', 'offer_types', 'furnizor'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $request->validate([
            'category_id' => 'required',
            'product_name.*' => 'required',
            'description.*' => 'required',
            'sp.*.price' => 'required',
            'sp.*.price_buing' => 'required',
        ]);
        $model = new $this->model();
        $model->save();

        foreach ($request->product_name as $key => $lang) {
            $arr = array(
                'article_id' => $model->id,
                'name' => $lang ? $lang : null,
                'description' => $request['description'][$key],
                'meta_name' => $request['meta_name'][$key],
                'meta_description' => $request['meta_description'][$key],
                'delivery' => $request['delivery'][$key],
                'mounting' => $request['mounting'][$key],
                'returning' => $request['returning'][$key],
                'lang_id' => $key,
                'model_name' => $this->model_name,
            );
            $this->translation->create($arr);
        }
        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->product_name['ro'] . '-' . $model->id);
        $new->price = $request->price;
        $new->furnizor_id = $request->furnizor_id;
        $new->category_id = $request->category_id;
        $new->offer_type_promotie = $request['offer_type_id']['promotie'];
        $new->offer_type_populare = $request['offer_type_id']['populare'];
        $new->offer_type_new = $request['offer_type_id']['noi'];
        $new->credit_zero = $request['offer_type_id']['credit_zero'];
        $new->no_stock = $request['offer_type_id']['no_stock'];
        $new->save();

//        GENERATE SKUS CODE
        $oldprodct = ProductSpecification::orderBy('id', 'desc')->first();
//        GENERATE SKUS CODE

        //----------
        foreach ($request->sp as $k => $spec) {
            $speci = new ProductSpecification();
            $speci->product_id = $model->id;
            $speci->sku = $spec['spec_sku'];
            $speci->locale_sku = !empty($oldprodct) ? '100' . $oldprodct->id + $k : 1001;
            $speci->price = $spec['price'];
            $speci->price_buing = $spec['price_buing'];

            if (!empty($spec['image'])) {
                $image = $spec['image'];
                $name = Carbon::now()->microsecond . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                Image::make($spec['image'])->save(public_path('img/products/') . $name);
                $speci->image = '/img/products/' . $name;
            }
            $speci->save();
            foreach ($spec['specs'] as $sp) {
                $pivot = new ProductSpecificationPivot();
                $pivot->spec_id = $speci->id;
                $pivot->attribute_id = $sp['specs'];
                $pivot->specification_id = $sp['sub_specs'];
                $pivot->product_id = $model->id;
                $pivot->save();
            }
        }

        if ($request->labels != null) {
            foreach ($request->labels as $label) {
                $lab = new ProductLabels();
                $lab->product_id = $model->id;
                $lab->label_id = $label;
                $lab->save();
            }
        }

        return response(['status' => 200, 'prod_id' => $model->id]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'product_name.*' => 'required',
            'description.*' => 'required',
            'sp.*.price' => 'required',
            'sp.*.price_buing' => 'required',
        ]);
        $model = $this->model::findOrFail($request->product_id);

        foreach ($model->transMany as $item) {
            $item->delete();
        }

        foreach ($request->product_name as $key => $lang) {
            $arr = array(
                'article_id' => $model->id,
                'name' => $lang ? $lang : null,
                'description' => $request['description'][$key],
                'meta_name' => $request['meta_name'][$key],
                'meta_description' => $request['meta_description'][$key],
                'delivery' => $request['delivery'][$key],
                'mounting' => $request['mounting'][$key],
                'returning' => $request['returning'][$key],
                'lang_id' => $key,
                'model_name' => $this->model_name,
            );
            $this->translation->create($arr);
        }

        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->product_name['ro'] . '-' . $model->id);
        $new->price = $request->price;
        $new->furnizor_id = $request->furnizor_id;
        $new->category_id = $request->category_id;
        $new->offer_type_promotie = $request['offer_type_id']['promotie'];
        $new->offer_type_populare = $request['offer_type_id']['populare'];
        $new->offer_type_new = $request['offer_type_id']['noi'];
        $new->credit_zero = $request['offer_type_id']['credit_zero'];
        $new->no_stock = $request['offer_type_id']['no_stock'];
        $new->save();

        //----------

        $storeOldSpecIds = array();

        foreach (ProductSpecification::where('product_id', $request->product_id)->get() as $item) {
            $storeOldSpecIds[] = $item->id;
            $item->forceDelete();
        }

        foreach (ProductSpecificationPivot::where('product_id', $request->product_id)->get() as $item) {
            $item->forceDelete();
        }

        $storeNewSpecIds = array();
//        GENERATE SKUS CODE
        $oldprodct = ProductSpecification::orderBy('id', 'desc')->first();
//        GENERATE SKUS CODE
        foreach ($request->sp as $k => $spec) {
            $speci = new ProductSpecification();
            $speci->product_id = $model->id;
            $speci->sku = $spec['spec_sku'];
            $speci->locale_sku = !empty($oldprodct) ? '100' . $oldprodct->id + $k : 1001;
            $speci->price = $spec['price'];
            $speci->price_buing = $spec['price_buing'];
            if (strpos($spec['image'], '/img/products/') !== false) {
                $speci->image = $spec['image'];
            } else {
                if (!empty($spec['image'])) {
                    $image = $spec['image'];
                    $name = Carbon::now()->microsecond . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                    Image::make($spec['image'])->save(public_path('img/products/') . $name);
                    $speci->image = '/img/products/' . $name;
                }
            }
            $speci->save();

            self::galResetIds($storeOldSpecIds[$k], $speci->id);


            foreach ($spec['specs'] as $sp) {
                $pivot = new ProductSpecificationPivot();
                $pivot->spec_id = $speci->id;
                $pivot->attribute_id = $sp['specs'];
                $pivot->specification_id = $sp['sub_specs'];
                $pivot->product_id = $model->id;
                $pivot->save();
            }
        }

        foreach (ProductLabels::where('product_id', $request->product_id)->get() as $item) {
            $item->delete();
        }

        foreach ($request->labels as $label) {
            $lab = new ProductLabels();
            $lab->product_id = $model->id;
            $lab->label_id = $label;
            $lab->save();
        }


        return response(['status' => 200]);
    }

    private function galResetIds($oldId, $newId)
    {
        $gal = ProductGallery::where('product_id', $oldId)->get();
        foreach ($gal as $key => $item) {
            $item->product_id = $newId;
            $item->save();
        }
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        $relation = $model->transMany;
        $spec = $model->productSpecifications;
        foreach ($relation as $key => $trans) {
            $trans->delete();
        }

        foreach ($spec as $k => $specification) {
            $specification->delete();
        }

        $model->delete();

        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }

    public function specs(Request $request)
    {
        $specs = Specification::where('parent_id', $request->specs)->get();
        return response(['status' => 200, 'specs' => $specs]);
    }

    public function specsGrouped(Request $request)
    {
        $arr = collect($request->form);

        $specs = array();
        foreach ($arr as $key => $item) {
            foreach ($item['sub_specs'] as $i) {
                $specs[] += $i;
            }
        }
        $dataList = collect($specs)->unique();

        $specs = Specification::whereIn('id', $dataList)
            ->with(['parent', 'lang'])
            ->select(['parent_id', 'id'])
            ->get()
            ->groupBy(function ($item, $key) {
                return Specification::findOrFail($item->parent_id)->lang->name;
            });
//        dd($specs);
        $name = array();
        $da = array();
        foreach ($specs as $key => $item) {
            foreach ($item as $k => $kk) {
                $name[$key][] = Specification::findOrFail($kk->id)->lang->name;
            }
        }

        $result = array();
        foreach ($name as $kkk => $na) {
            $result[] = '<span>' . $kkk . ':' . implode(',', $na) . '</span>';
        }

        $result = implode($result);


        return response(['status' => 200, 'specs' => $result]);
    }

    public function getCat(Request $request)
    {
        $childs = Category::where('parent_id', $request->category_id)->get();

        return response(['status' => 200, 'childs' => $childs]);
    }

    public function uploadGallery(Request $request, $product_id, $index)
    {
        if ($request->hasFile('file')) {
            $new = new ProductGallery();
            $dir = '/images/products/';
            $extension = strtolower($request->file('file')->getClientOriginalExtension()); // get image extension
            $fileName = Carbon::now()->microsecond . '.' . $extension; // rename image
            $request->file('file')->move(public_path($dir), $fileName);
            $new->name = $fileName;
            $new->url = $dir . $fileName;
            $new->product_id = $product_id;
            $new->save();

            $fileList = ProductGallery::where('product_id', $product_id)->get();
            return response(['status' => 200, 'fileList' => $fileList, 'index' => $index]);
        }

    }

    public function delUploadGallery(Request $request)
    {
        $gal = ProductGallery::findOrFail($request->id);
        $path = public_path('/images' . '/products' . '/' . $request->name);
        File::delete($path);
        $gal->delete();

        return response(['status' => 200]);
    }
}
