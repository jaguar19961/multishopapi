<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blog;
use App\Http\Controllers\Controller;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    protected $model;
    protected $model_name;

    public function __construct(Blog $model,  Translation $translation)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->model_name = 'App\\Models\\Blog::class';
    }

    public function index()
    {
        $model = $this->model->orderBy('created_at', 'desc')->get();
        return view('auth.pages.blog.index', compact('model'));
    }

    public function create(){
        return view('auth.pages.blog.create');
    }

    public function show($id)
    {
        $model = $this->model->findOrFail($id);
        return view('auth.pages.blog.show', compact('model'));
    }

    public function store(Request $request)
    {

        $input = $request->all();
        $model = $this->model::create($input);
        foreach ($request->name as $key => $lang) {
            $arr = array(
                'article_id' =>  $model->id,
                'name' =>  $lang ? $lang : null,
                'description' =>  $request->description[$key],
                'meta_name' =>  $request->meta_name[$key],
                'meta_description' =>  $request->meta_description[$key],
                'lang_id' => $key,
                'model_name' => $this->model_name,
            );
            $this->translation->create($arr);
        }

        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro'] . '-' . $model->id);
        if ($request->hasFile('image')) {
            $dir = '/img/blog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully Created!');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $model = $this->model::findOrFail($request->id);
        foreach ($request->name as $key => $lang) {
            $productNameTranslation = Translation::where('model_name', $this->model_name)->where('article_id', $request->id)->where('lang_id', $key)->first();
            $productNameTranslation->article_id = $request->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->description = $request['description'][$key];
            $productNameTranslation->meta_name = $request['meta_name'][$key];
            $productNameTranslation->meta_description = $request['meta_description'][$key];
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->model_name = $this->model_name;
            $productNameTranslation->save();
        }
        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro'] . '-' . $model->id);
        if ($request->hasFile('image')) {
            $dir = '/img/about/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        $model->delete();

        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }
}
