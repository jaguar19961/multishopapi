<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Script;
use Illuminate\Http\Request;

class ScriptController extends Controller
{
    public function index()
    {
        $model = Script::orderBy('created_at', 'desc')->get();
        return view('auth.pages.script.index', compact('model'));
    }


    public function store(Request $request){
        $model = Script::findOrFail(1);
        $model->name = $request->name;
        $model->save();
        return redirect()->back();
    }

    public function show($id){
        $model = Script::findOrFail($id);
        return view('auth.pages.script.show', compact('model'));
    }
}
