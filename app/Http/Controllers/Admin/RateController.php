<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Rate;
use Illuminate\Http\Request;

class RateController extends Controller
{
    public function save(Request $request){
        $request->validate([
            'product_id' => 'required',
            'description' => 'required',
            'name' => 'required',
            'rate' => 'required',
        ]);

        $rate = new Rate();
        $rate->product_id = $request->product_id;
        $rate->description = $request->description;
        $rate->name = $request->name;
        $rate->rate = $request->rate;
        $rate->save();
        return response(['status' => 200]);
    }

    public function getByProduct($product_id)
    {
        $rates = Rate::where('product_id', $product_id)->where('state', 1)->get();
        return response(['status' => 200, 'rates' => $rates]);
    }
}
