<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    protected $model;

    public function __construct(Gallery $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $model = $this->model->orderBy('created_at', 'desc')->get();
        return view('auth.pages.gallery.index', compact('model'));
    }

    public function create()
    {
        return view('auth.pages.gallery.create');
    }

    public function show($id)
    {
        $model = $this->model->findOrFail($id);
        return view('auth.pages.gallery.show', compact('model'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $model = $this->model::create($input);

        $new = $this->model->findOrFail($model->id);
        if ($request->hasFile('image')) {
            $dir = '/img/gallery/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully Created!');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $model = $this->model::findOrFail($request->id);
        $input = $request->all();

        $model->fill($input)->save();
        $new = $this->model->findOrFail($model->id);
        if ($request->hasFile('image')) {
            $dir = '/img/gallery/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        $model->delete();

        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }
}
