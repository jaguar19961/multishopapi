@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Recensy</h3>
{{--                    <a href="{{route('admin.testimonials.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>--}}
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Name</th>
                            <th scope="col" class="sort" data-sort="name">Description</th>
                            <th scope="col" class="sort" data-sort="name">Rate</th>
                            <th scope="col" class="sort" data-sort="name">Product</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                        <tr>
                            <td class="budget">
	                            {{$item->name}}
                            </td>
                            <td class="budget">
                                {{$item->description}}
                            </td>
                            <td class="budget">
                                {{$item->rate}}
                            </td>
                            <td class="budget">
                                @if($item->product)
                                <a href="{{url('/product/'.$item->product->slug)}}">{{$item->product->lang->name}}</a>
                                    @endif
                            </td>
                            <td class="budget">
                                {{$item->created_at->format('d-m-Y')}}
                            </td>

                            <td class="budget">
                                <a href="{{route('admin.testimonials.state', $item->id)}}" class="btn btn-sm btn-info  mr-4 ">@if($item->state === 1) Block @else Unblock @endif</a>
                                <a href="{{route('admin.testimonials.destroy', $item->id)}}" class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
