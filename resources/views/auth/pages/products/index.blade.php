@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <form role="form" action="{{route('admin.products.search')}}" method="POST"
                  class="navbar-search navbar-search-light form-inline mr-sm-3 mb-5" id="navbar-search-main">
                @csrf
                <div class="form-group mb-0">
                    <div class="input-group input-group-alternative input-group-merge">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input class="form-control" placeholder="Search" type="text" name="search" value="{{old('search')}}">
                    </div>
                </div>
                <a href="{{route('admin.products')}}" class="btn btn-warning ml-4 mr-4 ">Sterge filtrul</a>
            </form>

            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Products</h3>
                    <a href="{{route('admin.products.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Name</th>
{{--                            <th scope="col" class="sort" data-sort="name">SKU Furnizor</th>--}}
                            <th scope="col" class="sort" data-sort="name">Furnizor</th>
                            <th scope="col" class="sort" data-sort="name">Furnizor phone</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                            <tr>
                                <td class="budget">
                                    {{$item->lang ? $item->lang->name : ''}}
                                </td>
{{--                                <td class="budget">--}}
{{--                                    @foreach($item->productSpecifications as $sk)--}}
{{--                                       <span>{{$sk->sku}} &nbsp;&nbsp;</span>--}}
{{--                                    @endforeach--}}
{{--                                </td>--}}
                                <td class="budget">
                                    {{$item->furnizor ? $item->furnizor->name : 'Nu este furnizor'}}
                                </td>
                                <td class="budget">
                                    {{$item->furnizor ? $item->furnizor->phone : 'Nu este furnizor'}}
                                </td>

                                <td class="budget">
                                    {{$item->created_at->format('d-m-Y')}}
                                </td>

                                <td class="budget">
                                    <a data-toggle="modal" data-target="#exampleModal{{$item->id}}" href="?"
                                       class="btn btn-sm btn-info  mr-4 ">Detail</a>
                                    <a href="{{route('admin.products.show', $item->id)}}"
                                       class="btn btn-sm btn-info  mr-4 ">Edit</a>
                                    <a href="{{route('admin.products.destroy', $item->id)}}"
                                       class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                                </td>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1"
                                     aria-labelledby="exampleModalLabel{{$item->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel{{$item->id}}">Product
                                                    info</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div>
                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-2"><span>SKU</span></div>
                                                            <div class="col-2"><span>Image</span></div>
                                                            <div class="col-2"><span>Name</span></div>
                                                            <div class="col-2"><span>Atributes</span></div>
                                                            <div class="col-2"><span>Pret vinzare</span></div>
                                                            <div class="col-2"><span>Pret cumparare</span></div>
                                                            <div class="col-12">
                                                                <hr>
                                                            </div>
                                                            @foreach($item->productSpecifications as $it)
                                                                <div class="col-2"><span>{{$it->sku}}</span></div>
                                                                <div class="col-2"><span><img src="{{$it->image}}"
                                                                                              style="max-width: auto; max-height: 100px"
                                                                                              alt=""></span></div>
                                                                <div class="col-2">
                                                                    <span>{{$item->lang ? $item->lang->name : ''}}</span>
                                                                </div>
                                                                <div class="col-2"><span>@foreach($it->prodSpecs as $spec)
                                                                            ({{$spec->attribute ? $spec->attribute->parent->lang->name : ''}}
                                                                            : {{$spec->attribute ? $spec->attribute->lang->name : ''}}),
                                                                            <br> @endforeach</span></div>
                                                                <div class="col-2"><span>{{$it->price}}</span></div>
                                                                <div class="col-2"><span>{{$it->price_buing}}</span>
                                                                </div>
                                                                <div class="col-12">
                                                                    <hr>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        @endforeach
                        @if($model->count() === 0)
                            <tr>
                                <td><span>Nu sa gasit nici un rezultat!</span></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
               <div class="pagin_old col-12 col-md-12 m-auto mb-5 pb-3 pt-2">
                   {{$model->render()}}
               </div>
            </div>
        </div>
    </div>
@endsection
