@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0">
                    <h3 class="text-white mb-0">{{$model->name}}</h3>
                </div>
            </div>
            <admin-edit-products :model="{{json_encode($model)}}"
                                 :langs="{{json_encode(LaravelLocalization::getSupportedLocales())}}"
                                 :lang="{{json_encode(Lang::locale())}}"
                                 :action="{{json_encode(route('admin.specification.store'))}}"
                                 :categories="{{json_encode($categories)}}"
                                 :specifications="{{json_encode($specifications)}}"
                                 :offertypes="{{json_encode($offer_types)}}"
                                 :parent_category="{{json_encode($parent_category)}}"
                                 :furnizor="{{json_encode($furnizor)}}"
                                 :labels="{{json_encode($labels)}}"></admin-edit-products>
        </div>
    </div>
@endsection
