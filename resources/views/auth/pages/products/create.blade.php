@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0">
                    <h3 class="text-white mb-0">Product create</h3>
                </div>
            </div>

            <admin-products :langs="{{json_encode(LaravelLocalization::getSupportedLocales())}}"
                            :lang="{{json_encode(Lang::locale())}}"
                            :action="{{json_encode(route('admin.specification.store'))}}"
                            :categories="{{json_encode($categories)}}"
                            :specifications="{{json_encode($specifications)}}"
                            :offertypes="{{json_encode($offer_types)}}"
                            :furnizor="{{json_encode($furnizor)}}"
                            :labels="{{json_encode($labels)}}"></admin-products>

        </div>
    </div>
@endsection
