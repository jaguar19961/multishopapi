@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Category <strong class="text-underline">{{$parent_name->lang->name}}</strong></h3>
                    <div>
                        <a href="{{route('admin.category.show', $parent_name->id)}}" class="btn btn-sm btn-info  mr-4 ">Edit this category</a>
                        <a href="{{route('admin.category.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Name</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="status">Parent</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                            <tr>
                                <td class="budget">
                                    {{$item->lang->name}}
                                </td>
                                <td class="budget">
                                    {{$item->parent ? $item->parent->lang->name : 'Not have parent'}}
                                </td>
                                <td class="budget">
                                    {{$item->created_at->format('d-m-Y')}}
                                </td>

                                <td class="budget">
                                    <div class="d-flex">
{{--                                        <admin-assign-attribute :selected="{{json_encode($item->attrib->pluck('attribute_id')->toArray())}}" :category_id="{{json_encode($item->id)}}" :specifications="{{json_encode($attributes)}}"></admin-assign-attribute>--}}
                                        <a href="{{route('admin.category.show', $item->id)}}" class="btn btn-sm btn-info  mr-4 ">Edit</a>
                                        <a href="{{route('admin.category.destroy', $item->id)}}" class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                                    </div>
                                </td>

                            </tr>
                            @foreach($item->childs as $child)
                                <tr style="background-color: #0b1c39">
                                    <td class="budget">
                                        <i class="fa fa-arrow-right"></i>
                                        {{$child->lang->name}}
                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">
                                        {{$child->created_at->format('d-m-Y')}}
                                    </td>

                                    <td class="budget">
                                        <a href="{{route('admin.category.show', $child->id)}}" class="btn btn-sm btn-info  mr-4 ">Edit</a>
                                        <a href="{{route('admin.category.destroy', $child->id)}}" class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
