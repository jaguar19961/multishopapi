@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0">
                    <h3 class="text-white mb-0">Page create</h3>
                </div>
            </div>
            <div class="col-xl-12 order-xl-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Create new page</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('admin.pages.store')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <h6 class="heading-small text-muted mb-4">User information</h6>
                            <div class="pl-lg-4">
                                <div class="accordion" id="accordionExample">
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <div class="card">
                                            <div class="card-header" id="heading{{$localeCode}}">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link btn-block text-left" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse{{$localeCode}}" aria-expanded="true"
                                                            aria-controls="collapse{{$localeCode}}">
                                                        Text spre completare - [{{$localeCode}}]
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapse{{$localeCode}}"
                                                 class="collapse {{$localeCode === Lang::locale() ? 'show' : ''}}"
                                                 aria-labelledby="heading{{$localeCode}}"
                                                 data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <div class="pl-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label class="form-control-label"
                                                                           for="input-username">Name [{{$localeCode}}]</label>
                                                                    <input type="text" id="input-username"
                                                                           class="form-control" placeholder="Name"
                                                                           name="name[{{$localeCode}}]">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label class="form-control-label" for="input-email">Meta
                                                                        name [{{$localeCode}}]</label>
                                                                    <input type="text" id="input-email"
                                                                           class="form-control" placeholder="Meta name"
                                                                           name="meta_name[{{$localeCode}}]" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label class="form-control-label"
                                                                           for="input-first-name">Meta
                                                                        description [{{$localeCode}}]</label>
                                                                    <input type="text" id="input-first-name"
                                                                           class="form-control"
                                                                           placeholder="Meta description"
                                                                           name="meta_description[{{$localeCode}}]" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class="form-control-label">About blog [{{$localeCode}}]</label>
                                                                    <textarea required rows="4" name="description[{{$localeCode}}]"
                                                                              class="form-control ckeditor"
                                                                              placeholder="A few words about you ..."></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-last-name">Image</label>
                                            <div class="custom-file">
                                                <input type="file" name="image" class="custom-file-input"
                                                       id="customFileLang" lang="en" required>
                                                <label class="custom-file-label" for="customFileLang">Select
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4"/>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
