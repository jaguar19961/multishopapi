@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Furnizori</h3>
                    <a href="{{route('admin.brand.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Name</th>
                            <th scope="col" class="sort" data-sort="name">Phone</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                        <tr>
                            <td class="budget">
	                            {{$item->name}}
                            </td>
                            <td class="budget">
                                {{$item->phone}}
                            </td>
                            <td class="budget">
                                {{$item->created_at->format('d-m-Y')}}
                            </td>

                            <td class="budget">
                                <a href="{{route('admin.brand.show', $item->id)}}" class="btn btn-sm btn-info  mr-4 ">Edit</a>
                                <a href="{{route('admin.brand.destroy', $item->id)}}" class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
