<div class="navbar-inner">
    <!-- Collapse -->
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Nav items -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.home' ? 'active' : ''}}"
                   href="{{url('/admin/dashboard')}}">
                    <i class="ni ni-tv-2 text-primary"></i>
                    <span class="nav-link-text">Dashboard</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.invoice' ? 'active' : ''}}"
                   href="{{url('/admin/invoice')}}">
                    <i class="ni ni-atom text-primary"></i>
                    <span class="nav-link-text">Invoice</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.products' ? 'active' : ''}}"
                   href="{{url('/admin/products')}}">
                    <i class="ni ni-app text-primary"></i>
                    <span class="nav-link-text">Products</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.blog' ? 'active' : ''}}"
                   href="{{url('/admin/blog')}}">
                    <i class="ni ni-single-copy-04 text-primary"></i>
                    <span class="nav-link-text">Blog</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.pages' ? 'active' : ''}}"
                   href="{{url('/admin/pages')}}">
                    <i class="ni ni-single-copy-04 text-primary"></i>
                    <span class="nav-link-text">Pages</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.testimonials' ? 'active' : ''}}"
                   href="{{url('/admin/testimonial')}}">
                    <i class="ni ni-single-02 text-primary"></i>
                    <span class="nav-link-text">Recenzii</span>
                </a>
            </li>
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link {{Route::currentRouteName() === 'admin.category' ? 'active' : ''}}"--}}
{{--                   href="{{url('/admin/category')}}">--}}
{{--                    <i class="ni ni-palette text-primary"></i>--}}
{{--                    <span class="nav-link-text">Category</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link {{Route::currentRouteName() === 'admin.specification' ? 'active' : ''}}"--}}
{{--                   href="{{url('/admin/specification')}}">--}}
{{--                    <i class="ni ni-spaceship text-primary"></i>--}}
{{--                    <span class="nav-link-text">Specification</span>--}}
{{--                </a>--}}
{{--            </li>--}}

            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.category' ? 'active' : ''}}"
                   href="#navbar-examples1"
                   data-toggle="collapse"
                   role="button"
                   aria-expanded="{{Route::currentRouteName() === 'admin.category' ? 'true' : 'false'}}"
                   aria-controls="navbar-examples1">
                    <i class="ni ni-palette text-primary"></i>
                    <span class="nav-link-text">Category</span>
                </a>
                <div class="collapse" id="navbar-examples1">
                    <ul class="nav nav-sm flex-column">
                        @foreach(\App\Models\Category::where('parent_id', 0)->get() as $category)
                            <li class="nav-item">
                                <a href="{{url('/admin/category/all/'.$category->id)}}" class="nav-link">
                                    <span class="sidenav-normal"> {{$category->lang->name}} </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link"
                   href="#navbar-examples"
                   data-toggle="collapse"
                   role="button"
                   aria-expanded="false"
                   aria-controls="navbar-examples">
                    <i class="ni ni-spaceship text-primary"></i>
                    <span class="nav-link-text">Specification</span>
                </a>
                <div class="collapse" id="navbar-examples">
                    <ul class="nav nav-sm flex-column">
                        @foreach(\App\Models\Specification::where('parent_id', 0)->get() as $specification)
                        <li class="nav-item">
                            <a href="{{url('/admin/specification/all/'.$specification->id)}}" class="nav-link">
                                <span class="sidenav-normal"> {{$specification->lang->name}} </span>
                            </a>
                        </li>
                            @endforeach
                    </ul>
                </div>
            </li>


            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.brand' ? 'active' : ''}}"
                   href="{{url('/admin/brand')}}">
                    <i class="ni ni-briefcase-24 text-primary"></i>
                    <span class="nav-link-text">Furnizori</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.label' ? 'active' : ''}}"
                   href="{{url('/admin/label')}}">
                    <i class="ni ni-zoom-split-in text-primary"></i>
                    <span class="nav-link-text">Labels</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.locals' ? 'active' : ''}}"
                   href="{{url('/admin/locals')}}">
                    <i class="ni ni-map-big text-primary"></i>
                    <span class="nav-link-text">Locals</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.credit' ? 'active' : ''}}"
                   href="{{url('/admin/credit')}}">
                    <i class="ni ni-books text-primary"></i>
                    <span class="nav-link-text">Credite</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.about' ? 'active' : ''}}"
                   href="{{url('/admin/about')}}">
                    <i class="ni ni-air-baloon text-primary"></i>
                    <span class="nav-link-text">About</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.gallery' ? 'active' : ''}}"
                   href="{{url('/admin/gallery')}}">
                    <i class="ni ni-glasses-2 text-primary"></i>
                    <span class="nav-link-text">Gallery</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{Route::currentRouteName() === 'admin.script' ? 'active' : ''}}"
                   href="{{url('/admin/script')}}">
                    <i class="ni ni-bullet-list-67 text-primary"></i>
                    <span class="nav-link-text">Scripts</span>
                </a>
            </li>


        </ul>
    </div>
</div>
